cd /usr/local/docker/nginx/app/drifting-bottle
rm -rf ./html
mkdir html
tar -zxvf package.tgz -C ./html

cd /usr/local/docker/nginx

docker-compose down
docker-compose up -d